package pl.codementors.collections;

import pl.codementors.collections.Models.Color;
import pl.codementors.collections.Models.Dragon;

/**
 * App use collections and lambdas to make actions on dragons
 */

public class Main {
    public static void main(String[] args) {
        DragonCatalogue dragons = new DragonCatalogue();
        Dragon dragon1 = new Dragon(Color.GOLD, 120, 140, "Złotuch");
        Dragon dragon2 = new Dragon(Color.BLACK, 110, 110, "Alkor");
        Dragon dragon3 = new Dragon(Color.GREEN, 500, 200, "Zielienina");
        Dragon dragon4 = new Dragon(Color.GOLD, 190, 150, "Badur");
        Dragon dragon5 = new Dragon(Color.BLACK, 1000, 400, "Czernina");

        dragons.addDragon(dragon1);
        dragons.addDragon(dragon2);
        dragons.addDragon(dragon3);
        dragons.addDragon(dragon4);
        dragons.addDragon(dragon5);

        //printing down all methods results to console
        System.out.println("Writing down all dragons");
        dragons.writeAllDragons();
        System.out.println();
        System.out.println("Writing down all dragons names");
        dragons.writeAllNames();
        System.out.println();
        System.out.println("Writing down dargons sorted alphabetically by name and giving color");
        dragons.writeAllSorted();
        System.out.println();
        System.out.println("Oldest dragon");
        System.out.println(dragons.oldestDragonAge());
        System.out.println();
        System.out.println("Largest wings range");
        System.out.println(dragons.biggestWingsRange());
        System.out.println();
        System.out.println("longest name chars no");
        System.out.println(dragons.longestNameCharsNo());
        System.out.println();
        System.out.println("Colors list according to method param");
        System.out.println(dragons.specificColor(Color.GOLD));
        System.out.println();
        System.out.println("Only dragons names");
        System.out.println(dragons.namesOnly());
        System.out.println();
        System.out.println("names in capital letters");
        System.out.println(dragons.namesInCapital());
        System.out.println();
        System.out.println("Sorted by natural order");
        System.out.println(dragons.naturalSorted());
        System.out.println();
        System.out.println("Age sorted");
        System.out.println(dragons.ageSorted());
        System.out.println();
        System.out.println("Age testing for all dragons");
        System.out.println(dragons.allDragonsOlderThan(300));
        System.out.println();
        System.out.println("Color testing for any dragon");
        System.out.println(dragons.isOneDragonAsColor(Color.GOLD));
        System.out.println();
        System.out.println("List of dragon eggs created on dragon base");
        System.out.println(dragons.eggsList());
        System.out.println();
        System.out.println("List of dragon eggs depends on parent wings range");
        System.out.println(dragons.eggsOnWingsRange(200));
    }
}
