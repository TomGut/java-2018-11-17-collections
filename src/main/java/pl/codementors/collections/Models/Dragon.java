package pl.codementors.collections.Models;

public class Dragon implements Comparable <Dragon>{
    private Color color;
    private int age;
    private int wingsRange;
    private String name;

    public Dragon(Color color, int age, int wingsRange, String name) {
        this.color = color;
        this.age = age;
        this.wingsRange = wingsRange;
        this.name = name;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getWingsRange() {
        return wingsRange;
    }

    public void setWingsRange(int wingsRange) {
        this.wingsRange = wingsRange;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Kolor: " +
                color +
                ", Wiek: " +
                age +
                ", Rozpiętość skrzydeł: "
                + wingsRange +
                ", Imię: " +
                name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Dragon dragon = (Dragon) o;

        if (color != dragon.color) return false;
        if (age != dragon.age) return false;
        if (wingsRange != dragon.wingsRange) return false;
        return name != null ? name.equals(dragon.name) : dragon.name == null;
    }

    @Override
    public int hashCode() {
        int result = color != null ? color.hashCode() : 0;
        result = 31 * result + age;
        result = 31 * result + wingsRange;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public int compareTo(Dragon o) {
        int com = color.compareTo(o.color);
        if(com == 0){
            com = age - o.age;
        }
        if(com == 0){
            com = wingsRange - o.wingsRange;
        }
        if(com == 0){
            com = Integer.parseInt(name) - Integer.parseInt(o.name);
        }
        return com;
    }
}
