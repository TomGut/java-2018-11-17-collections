package pl.codementors.collections.Models;

public class DragonEgg {

    private Color color;
    private String name;

    public DragonEgg(Dragon dragon) {
        this.color = dragon.getColor();
        this.name = dragon.getName();
    }

    @Override
    public String toString() {
        return "DragonEgg{" + "color=" + color + ", name='" + name + '\'' + '}';
    }
}
