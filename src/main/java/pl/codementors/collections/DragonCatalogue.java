package pl.codementors.collections;

import pl.codementors.collections.Models.Color;
import pl.codementors.collections.Models.Dragon;
import pl.codementors.collections.Models.DragonEgg;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class DragonCatalogue {
    private List<Dragon> dragons = new ArrayList<>();

    public void addDragon(Dragon dragon){
       dragons.add(dragon);
    }

    public void removeDragon(Dragon dragon){
        dragons.remove(dragon);
    }

    public void writeAllDragons(){
       dragons.forEach(System.out::println);
    }

    public void writeAllNames(){
        dragons.
                stream().
                map(Dragon::getName).
                forEach(System.out::println);
    }

    /**
     * All dragons sorted by name and color
     */

    public void writeAllSorted(){
        dragons.stream().
                sorted(Comparator.
                        comparing(Dragon::getName).
                        thenComparing(Dragon::getColor)).
                forEach(dragon -> {
                    System.out.println(dragon.getName());
                    System.out.println(dragon.getColor());
                });
    }

    public int oldestDragonAge(){
        return dragons
                .stream()
                .mapToInt(Dragon::getAge)
                .max()
                .orElse(0);
    }

    public int biggestWingsRange(){
        return dragons
                .stream()
                .mapToInt(Dragon::getWingsRange)
                .max()
                .orElse(0);
    }

    public int longestNameCharsNo(){
        return dragons
                .stream()
                .map(Dragon::getName)
                .mapToInt(String::length)
                .max()
                .orElse(0);
    }

    /**
     * if @param color matches any dragon
     * @return list of dragons witch matching color
     */
    public List<Dragon> specificColor(Color color){
        return dragons
                .stream()
                .filter(dragon -> {
                    if(dragon.getColor().equals(color)){
                        return true;
                    }
                    return false;
                })
                .collect(Collectors.toList());
    }

    /**
     * @return list of dragosn names only
     */
    public List<String> namesOnly(){
        return dragons
                .stream()
                .map(Dragon::getName)
                .collect(Collectors.toList());
    }

    /**
     * @return list of dragons names in capital letters
     */
    public List<String> namesInCapital(){
        return dragons
                .stream()
                .map(Dragon::getName)
                .map(String::toUpperCase)
                .collect(Collectors.toList());
    }

    /**
     * @return list of dragons sorted in natural order
     */
    public List<Dragon> naturalSorted(){
        return dragons
                .stream()
                .sorted()
                .collect(Collectors.toList());
    }

    /**
     * @return list of dragons sorted by age
     */
    public List<Dragon> ageSorted(){
        return dragons
                .stream()
                .sorted(Comparator.comparingInt(Dragon::getAge))
                .collect(Collectors.toList());
    }

    /**
     * if @param age > all Dragons age
     * @return true
     */
    boolean allDragonsOlderThan(int age){
        return dragons
                .stream()
                .mapToInt(Dragon::getAge)
                .allMatch(value -> {
            if(age<value){
                return true;
            }
            return false;
        });
    }

    /**
     * if @param color match any single dragon
     * @return true
     */
    boolean isOneDragonAsColor(Color color){
        return dragons
                .stream()
                .map(Dragon::getColor)
                .anyMatch(c -> {
            if(c.equals(color)){
                return true;
            }
            return false;
        });
    }

    /**
     * @return list of all dragon eggs
     */
    public List<DragonEgg> eggsList(){
        return dragons.
                stream().
                map(DragonEgg::new).
                collect(Collectors.toList());
    }

    /**
     *
     * @param parentWingsRange >= Dragon wings range egg is created
     * @return list od DragonEgg
     */

    public List<DragonEgg> eggsOnWingsRange(int parentWingsRange){
        return dragons
                .stream()
                .filter(dragon -> {
            if(dragon.getWingsRange() >= parentWingsRange){
                return true;
            }
            return false;
        })
                .map(DragonEgg::new)
                .collect(Collectors.toList());
    }
}
